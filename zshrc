autoload -Uz compinit promptinit
promptinit
compinit

# enviromental variables
export PATH="$HOME/.poetry/bin:$PATH"
export EDITOR="/usr/bin/vim"
export VISUAL="/usr/bin/vim"
export TERM=xterm-256color

# antibody
source <(antibody init)
antibody bundle < ~/.zsh_plugins.txt

# bindkeys
bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[5~" beginning-of-history
bindkey "\e[6~" end-of-history
bindkey "\e[3~" delete-char
bindkey "\e[2~" quoted-insert
bindkey "\e[5C" forward-word
bindkey "\eOc" emacs-forward-word
bindkey "\e[5D" backward-word
bindkey "\eOd" emacs-backward-word
bindkey "\ee[C" forward-word
bindkey "\ee[D" backward-word
bindkey "^H" backward-delete-word
#     for rxvt
bindkey "\e[8~" end-of-line
bindkey "\e[7~" beginning-of-line

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# colors
eval $(dircolors -b ~/.dir_colors)
export YAOURT_COLORS="nb=1:pkg=1:ver=1;32:lver=1;45:installed=1;42:grp=1;34:od=1;41;5:votes=1;44:dsc=0:other=1;35"

# aliases
alias ls='ls --color=auto'
alias grep='grep --colour=auto'

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!

export PATH="/home/maglev/prj/builds/anaconda3/bin:$PATH"

. "/home/maglev/prj/builds/anaconda3/etc/profile.d/conda.sh"


# <<< conda initialize <<<
